# Salt Formula for defining PKGNG repo files

/usr/local/etc/pkg:
  file.directory:
    - user: root
    - group: wheel
    - mode: 0755

{% if salt['pillar.get']('pkgng:purge_repos') == true %}
/usr/local/etc/pkg/repos:
  file.absent:
    - path: /usr/local/etc/pkg/repos
{% endif %}

{% for repo, config in salt['pillar.get']('pkgng:repos')|dictsort %}
/usr/local/etc/pkg/repos/{{ repo }}.conf:
  file.managed:
    - user: root
    - group: wheel
    - mode: '0644'
    - makedirs: True
    - source: salt://pkgng/files/repo.conf
    - template: jinja
    - defaults:
        name: {{ repo }}
        protocol: {{ config.protocol }}
        packagehost: {{ config.packagehost }}
        repopath: {{ config.repopath }}
        enabled: {{ config.enabled|default(true) }}
        mirror_type: {{ config.mirror_type }}
        signature_type: {{ config.signature_type|default() }}
        pubkey: {{ config.pubkey|default() }}
        fingerprints: {{ config.fingerprints|default() }}
        ip_version: {{ config.ip_version|default() }}
        priority: {{ config.priority|default() }}
{% endfor %}
